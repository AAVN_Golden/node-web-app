'use strict';

const express = require('express');

// Constants
const PORT = 8080;

// App
var app = express();
var os = require("os");
var hostname = os.hostname();

app.get('/', function (req, res) {
  res.send('Hello world on sererID: ' + hostname);
});

app.listen(PORT);
console.log("Node server started");

